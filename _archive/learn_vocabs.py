# In this version, the vocabulary dictionary stores English words as keys and lists of German translations as values.
# The pick_random_word function now returns both the English word and its corresponding list of German translations.
# The script will then print all the German translations for the randomly chosen English word.

import random

def read_vocabulary(file_path):
    vocabulary = {}
    with open(file_path, 'r', encoding='utf-8') as file:
        for line in file:
            english, *german_translations = line.strip().split(',')
            english = english.strip()
            german_translations = [translation.strip() for translation in german_translations]
            vocabulary[english] = german_translations
    return vocabulary

def pick_random_word(vocabulary):
    english_word = random.choice(list(vocabulary.keys()))
    german_translations = vocabulary[english_word]
    return english_word, german_translations

def main():
    file_path = "vocab.txt"  # Update with your file path
    vocabulary = read_vocabulary(file_path)

    # print("Press space to reveal the translation. Any other key to exit.")
    
    while True:
        english_word = random.choice(list(vocabulary.keys()))
        go = input(f"\nEnglish Word: {english_word}\nPress space to reveal the translation. Any other key to exit.")
        if go == ' ':
            print("German Translations:", ', '.join(vocabulary[english_word]))
        else:
            break

if __name__ == "__main__":
    main()