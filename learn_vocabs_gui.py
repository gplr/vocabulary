import tkinter as tk
from tkinter import messagebox
import random
import sys

class VocabularyApp:
    def __init__(self, root, vocab_list):
        self.root = root
        self.vocab_list = vocab_list
        self.current_index = -1
        self.success_count = 0
        self.failure_count = 0

        self.exit_button = tk.Button(root, text="X", font=('Helvetica', 30), command=self.exit_program)
        self.exit_button.pack(side=tk.TOP, anchor=tk.NE)

        self.label1 = tk.Label(root, font=('Helvetica', 30), pady=20)
        self.label1.pack()

        self.label2 = tk.Label(root, text="Do you know the answer?", font=("Helvetica", 28), fg="#808080") 
        self.label2.pack()

        self.yes_button = tk.Button(root, text="Yes", command=self.on_yes)
        self.yes_button.pack(side="left", padx=10)
        
        self.no_button = tk.Button(root, text="No", command=self.on_no)
        self.no_button.pack(side="right", padx=10)

        self.next_button = tk.Button(root, text="Next Word", command=self.show_next_vocab)
        self.next_button.pack(side="bottom", pady=(0, 50))
        # self.next_button.pack(side="right", padx=(0, 10))

        self.success_label = tk.Label(root, text=f"Successful attempts: {self.success_count}", font=('Helvetica', 12),fg="green")
        self.success_label.pack(side=tk.LEFT, padx=(10, 5))

        self.failure_label = tk.Label(root, text=f"Unsuccessful attempts: {self.failure_count}", font=('Helvetica', 12), fg="red")
        self.failure_label.pack(side=tk.RIGHT, padx=(5, 10))

        self.show_button = tk.Button(root, text="Show Translation", command=self.show_translation)
        self.show_button.pack(side="bottom", pady=(0, 10))
        
        # self.reset_buttons()
        self.show_next_vocab()

    def show_translation(self):
        _, german_translations = self.vocab_list[self.current_index]
        self.label1.config(text=f"English: {self.vocab_list[self.current_index][0]}\nGerman: {', '.join(german_translations)}")

    def show_next_vocab(self):
        self.current_index = random.randint(0, len(self.vocab_list) - 1)
        self.label1.config(text=f"English: {self.vocab_list[self.current_index][0]}\nGerman: ???")
        self.reset_buttons()

    # def show_next_vocab(self):
    #     state_y = str(self.yes_button['state'])
    #     state_n = str(self.no_button['state'])
    #     # print the correct text!
    #     # Check if the user has selected 'yes' or 'no'
    #     if (state_y == 'active' and state_n == 'active'):
    #     # Display a message if no answer is selected
    #         messagebox.showinfo("Information", "Please select 'Yes' or 'No' before moving to the next vocabulary.")
    #     return

    #     # Update successful or unsuccessful attempts based on the selected answer
    #     #if self.yes_button.get():
    #     #    self.successful_attempts += 1
    #     #else:
    #     #    self.unsuccessful_attempts += 1

    #     # Reset answer buttons
        

    #     # Choose a random word for the next vocabulary
    #     self.current_index = random.randint(0, len(self.vocab_list) - 1)
    #     self.label1.config(text=f"English: {self.vocab_list[self.current_index][0]}\nGerman: ???")
    #     self.reset_buttons()

    #     # Update counter labels
    #     self.update_counter_labels()


    def reset_buttons(self):
        self.yes_button.config(state="active")
        self.no_button.config(state="active")

    def update_counters(self):
        self.success_label.config(text=f"Successful attempts: {self.success_count}")
        self.failure_label.config(text=f"Unsuccessful attempts: {self.failure_count}")

    def on_yes(self):
        self.yes_button.config(state="disabled")
        self.no_button.config(state="disabled")

        self.success_count += 1
        self.update_counters()

    def on_no(self):
        self.yes_button.config(state="disabled")
        self.no_button.config(state="disabled")

        self.failure_count += 1
        self.update_counters()
    
    def exit_program(self):
        sys.exit()

if __name__ == "__main__":
    # Replace this with the path to your vocabulary file
    file_path = "vocabs-long.txt"

    with open(file_path, 'r', encoding='utf-8') as file:
        vocab_lines = file.readlines()

    vocab_list = []
    for line in vocab_lines:
        parts = line.strip().split(" - ")
        if len(parts) == 2:
            english_word, german_translations = parts
            german_translations = [translation.strip() for translation in german_translations.split(',')]
            vocab_list.append((english_word, german_translations))

    if not vocab_list:
        messagebox.showerror("Error", "Invalid vocabulary file format.")
    else:
        root = tk.Tk()
        root.geometry("600x400")
        app = VocabularyApp(root, vocab_list)
        root.mainloop()
