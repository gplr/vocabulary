import random
import os

def read_vocabulary(file_path):
    vocabulary = {}
    with open(file_path, 'r', encoding='utf-8') as file:
        for line in file:
            line = line.strip()
            if line:
                parts = line.split(' - ')
                if len(parts) == 2:
                    english_word, german_translations_str = parts
                    german_translations = [translation.strip() for translation in german_translations_str.split(',')]
                    vocabulary[english_word] = german_translations
    return vocabulary

def clear_screen():
    os.system("clear")  # For Unix-based systems like macOS

def main():
    file_path = "vocabs-short.txt"  # Update with the actual path to your vocabulary file
    vocabulary = read_vocabulary(file_path)

    keys = list(vocabulary.keys())

    while True:
        random_key = random.choice(keys)
        clear_screen()
        print(f"\nThe english word is: {random_key}\n")
        print("Do you know the German translation? Think ...\n\n")
        input("Press Enter to reveal German translation...\n\n")
        
        print(f"German: {', '.join(vocabulary[random_key])}")

        # Ask to continue or exit
        action = input("\nPress Enter for the next vocabulary or type 'exit' to end: ")
        if action.lower() == 'exit':
            break

if __name__ == "__main__":
    main()
